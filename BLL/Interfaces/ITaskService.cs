using System.Collections.Generic;
using ProjectStructure.Models.DTO.Task;

namespace ProjectStructure.BLL.Interfaces
{
    public interface ITaskService
    {
        void AddNewTask(TaskCreateDTO item);
        void UpdateTask(TaskUpdateDTO item);
        TaskReadDTO GetTask(int Id);
        IEnumerable<TaskReadDTO> GetAllTasks();
        void DeleteTask(int Id);
    }
}