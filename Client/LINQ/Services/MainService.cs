using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using Newtonsoft.Json;
using LINQ.Models;
using LINQ.Models.DTO;
using Task = LINQ.Models.Task;

namespace LINQ.Services
{
    public class MainService 
    {
        
        public  static Dictionary<Project, int> GetTaskNumberByUserId(int userId)
        {
            return DeserializationService.Projects.ToDictionary(
                p => p,
                p => p.Tasks.Count(t => t.Performer.Id == userId));
        }

        public static List<Task> GetTasksListForUser(int userId)
        {
            const int taskNameSize = 45;
            return DeserializationService.Projects
                .SelectMany(t => t.Tasks)
                .Where(t => t.Performer.Id == userId && t.Name.Length < taskNameSize)
                .ToList();
        }

        public static List<(int id, string? name)> GetListOfFinishedTasksInCurrentYearForUser(int userId)
        {
            const int currentYear = 2021;
            return DeserializationService.Projects
                .SelectMany(p => p.Tasks)
                .Where(t => t.Performer.Id == userId && t.FinishedAt != null && t.FinishedAt.Value.Year == currentYear)
                .Select(t => (t.Id, t.Name))
                .ToList();

        }
        public static List<(int id, string name, List<User> users)> GetListOfTeamsSortedByRegistration()
        {
            const int ageLimit = 10;
            List<(int id, string name, List<User> users)> result2 = DeserializationService.Projects
                .Select(p => p.Team)
                .Where(t => t.Users.All(u => DateTime.Now.Year - u.Birthday.Year > ageLimit))
                .SelectMany(t => t.Users)
                .GroupBy(u => u.Team)
                .Select(u => (u.Key.Id, u.Key.Name, u.Key.Users.OrderBy(u => u.RegisteredAt).ToList()))
                .ToList();
             return result2;
        }

        public static List<(User user, List<Task> tasks)> GetUsersByAlphabetWithSortedTasks()
        {
            return DeserializationService.Projects
                .SelectMany(p => p.Tasks)
                .GroupBy(t => t.Performer)
                .OrderBy(t => t.Key.Firstname)
                .Select(t => (t.Key, t.Key.Tasks.OrderByDescending(t => t?.Name.Length).ToList()))
                .ToList();
        }

        public static Structure6 GetStructure6(int userId)
        {
            return DeserializationService.Projects
                .Select(p => p.Author)
                .Where(u => u.Id == userId)
                .Select(u => new Structure6()
                {
                    User = u
                })
                .GroupJoin(DeserializationService.Projects,
                    structure => structure.User.Id,
                        project => project.Author.Id,
                    (structure, project) =>
                    {
                        structure.LastProject = project
                            .OrderBy(p => p.CreatedAt).Last();
                        return structure;
                    }
                    )
                .GroupJoin(DeserializationService.Projects,
                    structure => structure.User.Id,
                    project => project.Author.Id,
                    (structure, project) =>
                    {
                        structure.TasksNumber = project
                            .OrderBy(p => p.CreatedAt)
                            .Last()
                            .Tasks
                            .Count();
                        return structure;
                    }
                    )
                .GroupJoin(DeserializationService.Projects,
                    structure => structure.User.Id,
                    project => project.Author.Id,
                    (structure, project) =>
                    {
                        structure.NotFinishedTasks = project
                            .Select(p => p.Author)
                            .First()
                            .Tasks
                            .Count(t => t.FinishedAt == null);
                        return structure;
                    })
                .GroupJoin(DeserializationService.Projects,
                    structure => structure.User.Id,
                    project => project.Author.Id,
                    (structure, project) =>
                    {
                        structure.LongestTask = project
                            .Select(p => p.Author)
                            .First()
                            .Tasks
                            .Where(t => t.FinishedAt != null)
                            .OrderBy(t => t.FinishedAt - t.CreatedAt).First();
                        return structure;
                    }).First();
        }

        public static Structure7 GetStructure7()
        {
            return DeserializationService.Projects
                .Select(p => new Structure7()
                {
                    Project = p
                }).Join(DeserializationService.Projects,
                        structure => structure.Project.Id,
                        project => project.Id,
                        (structure, project) =>
                        {
                            structure.LongestProjectTaskByDescription =
                                project.Tasks.OrderBy(t => t.Description).Last();
                            structure.ShortestProjectTaskByName = project.Tasks.OrderBy(t => t.Name).Last();
                            structure.NumberOfPerformancers = DeserializationService.Projects
                                                                  .Where(p => p.Id == project.Id &&
                                                                              (project.Tasks.Count() < 3 ||
                                                                               project.Description.Length > 20))
                                                                  ?.Count()
                                                              ?? 0;
                            return structure;
                        }
                    ).First();
        }
    }
}