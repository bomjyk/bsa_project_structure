using System.Collections.Generic;
using ProjectStructure.Models.Entities;

namespace ProjectStructure.Models.DTO.LINQ
{
    public class AlphabetUserWithTask
    {
        public Entities.User User;
        public List<Entities.Task> Tasks;
    }
}