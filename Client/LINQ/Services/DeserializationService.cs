using System.Net.Http;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LINQ.Models;
using LINQ.Models.DTO;
using Newtonsoft.Json;
using Task = LINQ.Models.Task;

namespace LINQ.Services
{
    public class DeserializationService
    {
        private static readonly string _apiRoute = "https://localhost:5001/api/";
        private static readonly string _apiTasksRoute = "Tasks";
        private static readonly string _apiProjectsRoute = "Projects";
        private static readonly string _apiTeamsRoute = "Teams";
        private static readonly string _apiUsersRoute = "Users";
        public static List<Project> Projects = new List<Project>();
        
        
        public static async System.Threading.Tasks.Task Initialize()
        {
            string projectsJson, tasksJson, usersJson, teamsJson;
            (projectsJson, tasksJson, usersJson, teamsJson) = await HttpCallFunction();
            List<ProjectDTO> projectDtos;
            List<TaskDTO> taskDtos;
            List<TeamDTO> teamDtos;
            List<UserDTO> userDtos;
            (projectDtos, taskDtos, teamDtos, userDtos) = DeserializeObjects(projectsJson, tasksJson, usersJson, teamsJson);
            GetProjectsList(projectDtos, taskDtos, teamDtos, userDtos);
        }

        private static void GetProjectsList(List<ProjectDTO> projectDtos, List<TaskDTO> taskDtos,
            List<TeamDTO> teamDtos, List<UserDTO> userDtos)
        {
            List<Team> teams = teamDtos.Select(t => new Team()
            {
                Id = t.Id,
                Name = t.Name,
                CreatedAt = t.CreatedAt,
                Users = new List<User>(),
                Projects = new List<Project>()
            }).ToList();
            List<User> users = userDtos.Select(u => new User()
            {
                Birthday = u.Birthday,
                Email = u.Email,
                Firstname = u.Firstname,
                Id = u.Id,
                Lastname = u.Lastname,
                RegisteredAt = u.RegisteredAt,
                Tasks = new List<Task>(),
                Team = null,
                Projects = new List<Project>()
            }).Join(teams,
            user => userDtos.Find(ud => ud.Id == user.Id).TeamId,
            teams => teams.Id,
            (user, teams) =>
            {
                teams.Users.Add(user);
                user.Team = teams;
                teams.Users.Find(u => u.Id == user.Id).Team = teams;
                return user;
            }).ToList();
            List<Task> tasks = taskDtos.Select(t => new Task()
            {
                Description = t.Description,
                Id = t.Id,
                Name = t.Name,
                State = t.State,
                CreatedAt = t.CreatedAt,
                FinishedAt = t.FinishedAt,
                
            }).Join(users,
                    tasks => taskDtos.Find(td => td.Id == tasks.Id).PerformerId,
                    users => users.Id,
                    (tasks, users) =>
                    {
                        users.Tasks.Add(tasks);
                        tasks.Performer = users;
                        return tasks;
                    }).ToList();
            List<Project> projects = projectDtos.Select(p => new Project()
            {
                Deadline = p.Deadline,
                Description = p.Description,
                Id = p.Id,
                Name = p.Name,
                CreatedAt = p.CreatedAt
            }).GroupJoin(tasks,
                project => project.Id,
                tasks => taskDtos.Find(td => td.Id == tasks.Id).ProjectId,
                (project, tasks) =>
                {
                    tasks.ToList().ForEach(t => t.Project = project);
                    project.Tasks.AddRange(tasks);
                    return project;
                }
            ).Join(teams,
                    project => projectDtos.Find(pd => pd.Id == project.Id).TeamId,
                    teams => teams.Id,
                    (project, teams) =>
                    {
                        project.Team = teams;
                        teams.Projects.Add(project);
                        return project;
                    })
                .Join(users,
                    project => projectDtos.Find(pd => pd.Id == project.Id).AuthorId,
                    users => users.Id,
                    (project, users) =>
                    {
                        project.Author = users;
                        users.Projects.Add(project);
                        return project;
                    }).ToList();
            Projects = projects;

        }
        
        private static  (List<ProjectDTO>, List<TaskDTO>, List<TeamDTO>, List<UserDTO>) 
            DeserializeObjects(string projects, string tasks, string users, string teams)
        {
            List<ProjectDTO> projectDtos = JsonConvert.DeserializeObject<List<ProjectDTO>>(projects);
            List<TaskDTO> taskDtos = JsonConvert.DeserializeObject<List<TaskDTO>>(tasks);
            List<TeamDTO> teamDtos = JsonConvert.DeserializeObject<List<TeamDTO>>(teams);
            List<UserDTO> userDtos = JsonConvert.DeserializeObject<List<UserDTO>>(users);
            return (projectDtos, taskDtos, teamDtos, userDtos);
        }
        private static async Task<(string, string, string, string)> HttpCallFunction()
        {
            string projectsJson, tasksJson, usersJson, teamsJson;
            projectsJson = await GetJsonByURL(_apiRoute + _apiProjectsRoute);
            tasksJson = await GetJsonByURL(_apiRoute + _apiTasksRoute);
            usersJson = await GetJsonByURL(_apiRoute + _apiUsersRoute);
            teamsJson = await GetJsonByURL(_apiRoute + _apiTeamsRoute);
            return (projectsJson, tasksJson, usersJson, teamsJson);
        }

        private static async Task<string> GetJsonByURL(string url)
        {
            string json;
            HttpClientHandler clientHandler = new HttpClientHandler();
            clientHandler.ServerCertificateCustomValidationCallback = (sender, cert, chain, sslPolicyErrors) => { return true; };
            using (HttpClient client = new HttpClient(clientHandler))
            {
                HttpResponseMessage tasksResponseMessage = await client.GetAsync(url);
                tasksResponseMessage.EnsureSuccessStatusCode();
                json = await tasksResponseMessage.Content.ReadAsStringAsync();
            }
            return json;
        }
    }
}