using System.Collections;
using System.Collections.Generic;
using ProjectStructure.Models.DTO.Team;

namespace ProjectStructure.BLL.Interfaces
{
    public interface ITeamService
    {
        void CreateTeam(TeamCreateDTO team);
        void DeleteTeam(int Id);
        void UpdateTeam(TeamUpdateDTO team);
        TeamReadDTO GetTeam(int Id);
        IEnumerable<TeamReadDTO> GetAllTeams();
    }
}