using System.Collections;
using System.Collections.Generic;
using ProjectStructure.Models.DTO.User;
using ProjectStructure.Models.Entities;

namespace ProjectStructure.BLL.Interfaces
{
    public interface IUserService
    {
        void RegisterUser(UserCreateDTO user);
        void UpdateUser(UserUpdateDTO user);
        UserReadDTO GetUser(int Id);
        IEnumerable<UserReadDTO> GetAllUsers();
        void DeleteUser(int Id);
    }
}