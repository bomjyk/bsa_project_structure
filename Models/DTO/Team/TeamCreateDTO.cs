namespace ProjectStructure.Models.DTO.Team
{
    public class TeamCreateDTO
    {
        public string? Name { get; set; }
    }
}