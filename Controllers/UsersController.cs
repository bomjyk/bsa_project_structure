using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using ProjectStructure.BLL.Interfaces;
using ProjectStructure.Models.DTO.User;

namespace ProjectStructure.Controllers
{
    [Route("api/[controller]")]
    public class UsersController : ControllerBase
    {
        private IUserService _userService;

        public UsersController(IUserService userService)
        {
            _userService = userService;
        }
        
        [HttpGet("id")]
        public ActionResult<UserReadDTO> GetUser(int Id)
        {
            try
            {
                return Ok(_userService.GetUser(Id));
            }
            catch
            {
                return BadRequest();
            }
        }

        [HttpGet]
        public ActionResult<IEnumerable<UserReadDTO>> GetUsers()
        {
            return Ok(_userService.GetAllUsers());
        }

        [HttpPost]
        public ActionResult AddUser(UserCreateDTO user)
        {
            try
            {
                _userService.RegisterUser(user);
                return Ok();
            }
            catch
            {
                return new BadRequestResult();
            }
        }

        [HttpPut]
        public ActionResult UpdateUser(UserUpdateDTO user)
        {
            _userService.UpdateUser(user);
            return Ok();
        }
        
        [HttpDelete]
        public ActionResult RemoveUser(int Id)
        {
            try
            {
                _userService.DeleteUser(Id);
                return new NoContentResult();
            }
            catch
            {
                return new BadRequestResult();
            }
        }
    }
}