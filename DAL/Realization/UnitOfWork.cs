using ProjectStructure.DAL.Interfaces;

namespace ProjectStructure.DAL.Realization
{
    public class UnitOfWork : IUnitOfWork
    {
        public IProjectRepository Projects { get; }
        public ITaskRepository Tasks { get; }
        public ITeamRepository Teams { get; }
        public IUserRepository Users { get; }

        public UnitOfWork(IProjectRepository projectRepository,
            ITaskRepository taskRepository,
            ITeamRepository teamRepository,
            IUserRepository userRepository)
        {
            Projects = projectRepository;
            Tasks = taskRepository;
            Teams = teamRepository;
            Users = userRepository;
        }
        public int Save()
        {
            return 1;
        }
    }
}