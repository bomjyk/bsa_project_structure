using Microsoft.Extensions.DependencyInjection;
using ProjectStructure.BLL.Interfaces;
using ProjectStructure.BLL.Services;
using ProjectStructure.DAL.Interfaces;
using ProjectStructure.DAL.Realization;

namespace ProjectStructure.Extensions
{
    public static class ServicesExtension
    {
        public static void AddRepositories(this IServiceCollection collection)
        {
            collection.AddSingleton<IProjectRepository, ProjectRepository>();
            collection.AddSingleton<ITaskRepository, TaskRepository>();
            collection.AddSingleton<ITeamRepository, TeamRepository>();
            collection.AddSingleton<IUserRepository, UserRepository>();
        }

        public static void AddServices(this IServiceCollection collection)
        {
            collection.AddTransient<IUserService, UserService>();
            collection.AddTransient<ITeamService, TeamService>();
            collection.AddTransient<ITaskService, TaskService>();
            collection.AddTransient<IProjectService, ProjectService>();
        }

        public static void AddUnitOfWork(this IServiceCollection collection)
        {
            collection.AddSingleton<IUnitOfWork, UnitOfWork>();
        }
    }
}