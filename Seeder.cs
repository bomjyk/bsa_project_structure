using System.Collections.Generic;
using System.IO;
using System.Linq;
using AutoMapper;
using ProjectStructure.DAL.Interfaces;
using Newtonsoft.Json;
using ProjectStructure.Models.DTO.Project;
using ProjectStructure.Models.Entities;

namespace ProjectStructure
{
    public class Seeder
    {
        private IProjectRepository _projectRepository;
        private ITaskRepository _taskRepository;
        private ITeamRepository _teamRepository;
        private IUserRepository _userRepository;
        private IMapper _mapper;

        public Seeder(IProjectRepository projectRepository,
            ITaskRepository taskRepository,
            ITeamRepository teamRepository,
            IUserRepository userRepository,
            IMapper mapper)
        {
            _projectRepository = projectRepository;
            _taskRepository = taskRepository;
            _teamRepository = teamRepository;
            _userRepository = userRepository;
            _mapper = mapper;
        }

        public void SeedData()
        {
            SeedProjects();
        }

        private void SeedProjects()
        {
            string json = ReadJsonFromFile(Directory.GetCurrentDirectory() + "projects.json");
            IEnumerable<Project> projects = _mapper.Map<IEnumerable<Project>>(JsonConvert.DeserializeObject<IEnumerable<ProjectReadDTO>>(json));
            projects.ToList().ForEach(p => _projectRepository.Create(p));
        }

        private string ReadJsonFromFile(string filepath)
        {
            using StreamReader sr = new StreamReader(filepath);
                string json = sr.ReadToEnd();
            return json;
        }
    }
}