using ProjectStructure.Models.Entities;

namespace ProjectStructure.DAL.Interfaces
{
    public interface IUserRepository : IRepository<User>
    {
        
    }
}