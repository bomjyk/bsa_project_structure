using ProjectStructure.Models.Entities;

namespace ProjectStructure.DAL.Interfaces
{
    public interface IProjectRepository : IRepository<Project>
    {
        
    }
}