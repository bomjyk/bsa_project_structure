using System.Collections.Generic;

namespace ProjectStructure.Models.DTO.LINQ
{
    public class TeamStructureSortedByRegistrationWithUserField
    {
        public int Id;
        public string Name;
        public List<Entities.User> Users = new List<Entities.User>();
    }
}