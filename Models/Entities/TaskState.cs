namespace ProjectStructure.Models.Entities
{
    public enum TaskState
    {
        ToDo,
        InProgress,
        Done,
        Rejected
    }
}