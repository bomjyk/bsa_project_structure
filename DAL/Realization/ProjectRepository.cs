using System.Collections.Generic;
using System.IO;
using System.Linq;
using Newtonsoft.Json;
using ProjectStructure.DAL.Interfaces;
using ProjectStructure.Models.Entities;

namespace ProjectStructure.DAL.Realization
{
    public class ProjectRepository : IProjectRepository
    {
        private List<Project> _db;

        public ProjectRepository()
        {
            _db = new List<Project>();
        }
        
        public IEnumerable<Project> GetAll()
        {
            return (IEnumerable<Project>)_db;
        }

        public Project Get(int Id)
        {
            return _db.Find(p => p.Id == Id);
        }

        public void Create(Project item)
        {
            item.Id = _db.Count;
            _db.Add(item);
        }

        public void Update(Project item)
        {
            var project = _db.FirstOrDefault(p => p.Id == item.Id);
            if (project != null) _db[_db.FindIndex(t => t.Id == item.Id)] = item;
        }

        public void Delete(Project item)
        {
            _db.Remove(item);
        }
    }
}