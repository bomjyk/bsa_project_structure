using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.OpenApi.Models;
using ProjectStructure.BLL.Services;
using ProjectStructure.DAL.Interfaces;
using ProjectStructure.DAL.Realization;
using ProjectStructure.Extensions;
using ProjectStructure.Models.DTO.Project;
using ProjectStructure.Models.DTO.Task;
using ProjectStructure.Models.DTO.Team;
using ProjectStructure.Models.DTO.User;
using ProjectStructure.Models.Entities;

namespace ProjectStructure
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers();
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo {Title = "ProjectStructure", Version = "v1"});
            });
            
            var map = MapperConfiguration().CreateMapper();
            services.AddScoped(_ => map);
            services.AddRepositories();
            services.AddUnitOfWork();
            services.AddServices();
            services.AddSingleton<LinqService>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseSwagger();
                app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "ProjectStructure v1"));
            }

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints => { endpoints.MapControllers(); });
        }

        MapperConfiguration MapperConfiguration()
        {
            var config = new MapperConfiguration(cnf =>
            {
                cnf.CreateMap<TeamCreateDTO, Team>()
                    .ForMember("Id", opt => opt.Ignore())
                    .ForMember("CreatedAt", opt => opt.Ignore())
                    .ReverseMap();
                cnf.CreateMap<TeamUpdateDTO, Team>()
                    .ForMember("CreatedAt", opt => opt.Ignore())
                    .ReverseMap();
                cnf.CreateMap<Team, TeamReadDTO>();
                cnf.CreateMap<UserCreateDTO, User>()
                    .ForMember("Id", opt => opt.Ignore())
                    .ForPath(u => u.Team.Id, opt => opt.MapFrom(dto => dto.TeamId))
                    .ReverseMap();
                cnf.CreateMap<UserUpdateDTO, User>()
                    .ForMember("Id", opt => opt.Ignore())
                    .ForMember("RegisteredAt", opt => opt.Ignore())
                    .ForPath(u => u.Team.Id, opt => opt.MapFrom(dto => dto.TeamId))
                    .ReverseMap();
                cnf.CreateMap<User, UserReadDTO>()
                    .ForMember(u => u.TeamId, opt => opt.MapFrom(u => u.Team.Id));
                cnf.CreateMap<TaskCreateDTO, ProjectStructure.Models.Entities.Task>()
                    .ForMember("Id", opt => opt.Ignore())
                    .ForPath(t => t.Performer.Id, opt => opt.MapFrom(dto => dto.PerformerId) )
                    .ForPath(t => t.Project.Id, opt => opt.MapFrom(dto => dto.ProjectId))
                    .ForMember("CreatedAt", opt=>opt.Ignore())
                    .ReverseMap();
                cnf.CreateMap<TaskUpdateDTO, ProjectStructure.Models.Entities.Task>()
                    .ForPath(t => t.Performer.Id, opt => opt.MapFrom(dto => dto.PerformerId) )
                    .ForPath(t => t.Project.Id, opt => opt.MapFrom(dto => dto.ProjectId))
                    .ForMember("CreatedAt", opt=>opt.Ignore())
                    .ReverseMap();
                cnf.CreateMap<ProjectStructure.Models.Entities.Task, TaskReadDTO>()
                    .ForMember(t => t.PerformerId, opt => opt.MapFrom(t => t.Performer.Id))
                    .ForMember(t => t.ProjectId, opt => opt.MapFrom(t => t.Project.Id));
                cnf.CreateMap<ProjectCreateDTO, Project>()
                    .ForMember("Id", opt => opt.Ignore())
                    .ForPath(t => t.Author.Id, opt => opt.MapFrom(dto => dto.AuthorId) )
                    .ForPath(t => t.Team.Id, opt => opt.MapFrom(dto => dto.TeamId))
                    .ForMember("CreatedAt", opt=>opt.Ignore())
                    .ReverseMap();
                cnf.CreateMap<ProjectUpdateDTO, Project>()
                    .ForPath(t => t.Author.Id, opt => opt.MapFrom(dto => dto.AuthorId) )
                    .ForPath(t => t.Team.Id, opt => opt.MapFrom(dto => dto.TeamId))
                    .ForMember("CreatedAt", opt=>opt.Ignore())
                    .ReverseMap();
                cnf.CreateMap<Project, ProjectReadDTO>()
                    .ForMember(p => p.AuthorId, opt => opt.MapFrom(p => p.Author.Id))
                    .ForMember(p => p.TeamId, opt => opt.MapFrom(p => p.Team.Id));
                cnf.CreateMap<ProjectReadDTO, Project>()
                    .ForPath(p => p.Author.Id, opt => opt.MapFrom(p => p.AuthorId))
                    .ForPath(p => p.Team.Id, opt => opt.MapFrom(p => p.TeamId))
                    .ReverseMap();
            });
            return config;
        }
    }
}