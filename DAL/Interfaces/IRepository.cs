using System.Collections.Generic;

namespace ProjectStructure.DAL.Interfaces
{
    public interface IRepository<T> where T : class
    {
        IEnumerable<T> GetAll();
        T Get(int Id);
        void Create(T item);
        void Update(T item);
        void Delete(T item);
    }
}