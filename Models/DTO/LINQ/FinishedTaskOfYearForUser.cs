namespace ProjectStructure.Models.DTO.LINQ
{
    public class FinishedTaskOfYearForUser
    {
        public int Id;
        public string? Name;
    }
}