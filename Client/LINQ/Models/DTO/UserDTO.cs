using System;

namespace LINQ.Models.DTO
{
    public class UserDTO
    {
        public int Id { get; set; }
        public int? TeamId { get; set; }
        public string? Firstname { get; set; }
        public string? Lastname { get; set; }
        public string? Email { get; set; }
        public DateTime RegisteredAt { get; set; }
        public DateTime Birthday { get; set; }
    }
}