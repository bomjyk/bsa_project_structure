using System;

namespace ProjectStructure.Models.DTO.Team
{
    public class TeamReadDTO
    {
        public int Id { get; set; }
        public string? Name { get; set; }
        public DateTime CreatedAt { get; set; }
    }
}