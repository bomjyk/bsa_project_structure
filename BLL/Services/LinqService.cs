using System;
using System.Collections.Generic;
using System.Linq;
using ProjectStructure.DAL.Interfaces;
using ProjectStructure.Models.DTO.LINQ;
using ProjectStructure.Models.Entities;

namespace ProjectStructure.BLL.Services
{
    public class LinqService
    {
        private ITaskRepository _taskRepository;
        private IProjectRepository _projectRepository;
        private ITeamRepository _teamRepository;
        private IUserRepository _userRepository;

        public LinqService(ITaskRepository taskRepository, 
            IProjectRepository projectRepository,
            ITeamRepository teamRepository,
            IUserRepository userRepository)
        {
            _taskRepository = taskRepository;
            _projectRepository = projectRepository;
            _teamRepository = teamRepository;
            _userRepository = userRepository;
        }
        
        public Dictionary<Project, int> GetTaskNumberByUserId(int userId)
        {
            return _projectRepository.
                GetAll().
                ToList()
                .Select(p =>
                (
                    p,
                    _taskRepository.GetAll().Where(t => t.Project.Id == p.Id).Count(t => t.Performer.Id == userId)
                ))
                .ToDictionary(
                    p => p.p,
                    p => p.Item2
                );
        }
        public List<Task> GetTasksListForUser(int userId)
        {
            const int taskNameSize = 45;
            return _taskRepository.GetAll()
                .Where(t => t.Performer.Id == userId && t.Name.Length < taskNameSize)
                .ToList();
        }
        public List<FinishedTaskOfYearForUser> GetListOfFinishedTasksInCurrentYearForUser(int userId)
        {
            const int currentYear = 2021;
            return _taskRepository.GetAll()
                .Where(t => t.Performer.Id == userId && t.FinishedAt != null && t.FinishedAt.Value.Year == currentYear)
                .Select(t => new FinishedTaskOfYearForUser()
                {
                    Id = t.Id, 
                    Name =  t.Name
                })
                .ToList();
        }
        public List<TeamStructureSortedByRegistrationWithUserField> GetListOfTeamsSortedByRegistration()
        {
            const int ageLimit = 10;
            return _userRepository
                .GetAll()
                .Where(u => DateTime.Now.Year - u.BirthDay.Year > ageLimit)
                .OrderBy(u => u.RegisteredAt)
                .GroupBy(u => u.Team)
                .Select(u => new TeamStructureSortedByRegistrationWithUserField()
                    {
                        Id = u.Key.Id,
                        Name = u.Key.Name,
                        Users = u.Where(t => t.Team == u.Key).Select(t => t).ToList()
                    })
                .ToList();
        }
        public List<AlphabetUserWithTask> GetUsersByAlphabetWithSortedTasks()
        {
            return _taskRepository.GetAll()
                .OrderByDescending(t =>
                {
                    if (t?.Name != null) return t?.Name.Length;
                    else return 0;
                })
                .GroupBy(t => t.Performer)
                .OrderBy(t => t.Key.FirstName)
                .Select(g => new AlphabetUserWithTask
                {
                    Tasks = g.Select(t => t).ToList(),
                    User = g.Key
                })
                .ToList();
        }

        public UserProjectAndTasks GetUserProjectAndTasks(int userId)
        {
            return _projectRepository.GetAll()
                .Select(p => p.Author)
                .Where(u => u.Id == userId)
                .Select(u => new UserProjectAndTasks()
                {
                    User = u,
                    LastProject = _projectRepository
                        .GetAll()
                        .Where(p => p.Author.Id == userId)
                        .OrderBy(p => p.CreatedAt)
                        .Last(),
                    TasksNumber = _taskRepository.GetAll().Where(t =>
                        _projectRepository
                        .GetAll()
                        .Where(p => p.Author.Id == userId)
                        .OrderBy(p => p.CreatedAt)
                        .Last().Id == t.Project.Id)
                        .Count(),
                    NotFinishedTasks = _taskRepository.GetAll()
                        .Where(t => t.Performer.Id == userId && t.FinishedAt == null)
                        .Count(),
                    LongestTask = _taskRepository.GetAll()
                        .Where(t => t.Performer.Id == userId)
                        .Where(t => t.FinishedAt != null)
                        .OrderBy(t => t.FinishedAt - t.CreatedAt).First()
                }).First();
        }

        public ProjectAndTaskStructure GetProjectAndTaskStructure()
        {
            return _projectRepository.GetAll()
                .Select(p => new ProjectAndTaskStructure()
                {
                    Project = p,
                    LongestProjectTaskByDescription = _taskRepository.GetAll()
                        .OrderBy(t => t.Description).Last(),
                    ShortestProjectTaskByName = _taskRepository.GetAll()
                        .OrderBy(t => t.Name).Last(),
                    NumberOfPerformancers = _projectRepository.GetAll()
                        .Where(pr => pr.Id == p.Id && 
                                     (_taskRepository.GetAll().Where(t => t.Project.Id == p.Id).Count() < 3 ||
                                      p.Description.Length > 20))
                        ?.Count() ?? 0
                })
                .First();
        }
    }
}