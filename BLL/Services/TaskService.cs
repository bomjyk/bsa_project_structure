using System;
using System.Collections.Generic;
using AutoMapper;
using ProjectStructure.BLL.Interfaces;
using ProjectStructure.DAL.Interfaces;
using ProjectStructure.DAL.Realization;
using ProjectStructure.Models.DTO.Task;
using ProjectStructure.Models.Entities;

namespace ProjectStructure.BLL.Services
{
    public class TaskService : ITaskService
    {
        private IUnitOfWork _unitOfWork;
        private IMapper _mapper;

        public TaskService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }
        
        public void AddNewTask(TaskCreateDTO item)
        {
            Task task = _mapper.Map<Task>(item);
            task.CreatedAt = DateTime.Now;
            task.Performer = _unitOfWork.Users.Get(item.PerformerId);
            task.Project = _unitOfWork.Projects.Get(item.ProjectId);
            if (task.Performer == null || task.Performer == null) throw new Exception();
            _unitOfWork.Tasks.Create(task);
        }

        public void UpdateTask(TaskUpdateDTO item)
        {
            _unitOfWork.Tasks.Update(_mapper.Map<Task>(item));
        }

        public TaskReadDTO GetTask(int Id)
        {
            return _mapper.Map<TaskReadDTO>(_unitOfWork.Tasks.Get(Id));
        }

        public IEnumerable<TaskReadDTO> GetAllTasks()
        {
            return _mapper.Map<IEnumerable<TaskReadDTO>>(_unitOfWork.Tasks.GetAll());
        }

        public void DeleteTask(int Id)
        {
            Task task = _unitOfWork.Tasks.Get(Id);
            if (task != null)
            {
                _unitOfWork.Tasks.Delete(task);
            }
            else
            {
                throw new Exception();
            }
            
        }
    }
}