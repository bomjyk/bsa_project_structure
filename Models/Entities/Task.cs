using System;

namespace ProjectStructure.Models.Entities
{
    public class Task
    {
        public int Id { get; set; }
        public User Performer { get; set; }
        public Project Project { get; set; }
        public string? Name { get; set; }
        public string? Description { get; set; }
        public TaskState State { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime? FinishedAt { get; set; }
    }
}