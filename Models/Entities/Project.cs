using System;

namespace ProjectStructure.Models.Entities
{
    public class Project
    {
        public int Id { get; set; }
        public User Author { get; set; }
        public Team Team { get; set; }
        public string? Name { get; set; }
        public string? Description { get; set; }
        public DateTime Deadline { get; set; }
        public DateTime CreatedAt { get; set; }
    }
}