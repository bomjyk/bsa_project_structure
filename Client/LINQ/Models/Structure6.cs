namespace LINQ.Models
{
    public class Structure6
    {
        public User User { get; set; }
        public Project LastProject { get; set; }
        public int TasksNumber { get; set; }
        public int NotFinishedTasks { get; set; }
        public Task LongestTask { get; set; }
    }
}