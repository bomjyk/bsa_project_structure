using System;

namespace ProjectStructure.Models.DTO.Task
{
    public class TaskUpdateDTO
    {
        public int Id { get; set; }
        public int PerformerId { get; set; }
        public int ProjectId { get; set; }
        public string? Name { get; set; }
        public string? Description { get; set; }
        public int State { get; set; }
        public DateTime? FinishedAt { get; set; }
    }
}