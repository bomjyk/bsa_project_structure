using ProjectStructure.DAL.Realization;

namespace ProjectStructure.DAL.Interfaces
{
    public interface IUnitOfWork
    {
        IProjectRepository Projects { get; }
        ITaskRepository Tasks { get; }
        ITeamRepository Teams { get; }
        IUserRepository Users { get; }
        int Save();
    }
}