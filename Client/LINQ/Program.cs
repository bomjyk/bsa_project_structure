﻿using System;
using System.Linq;
using System.Threading.Tasks;
using LINQ.Services;

namespace LINQ
{
    class Program
    {
        static async Task Main(string[] args)
        {
            await DeserializationService.Initialize();

            bool closeFlag = false;
            Console.WriteLine("Hello =)");
            string menuString = "Menu:\n";
            MenuService.menu
                .Select( m => new {m.Key, m.Value.Method.Name})
                .ToList()
                .ForEach(m => menuString += m.Key + " " + m.Name + "\n");
            while (!closeFlag)
            {
                Console.WriteLine("Menu");
                Console.WriteLine(menuString);
                Console.WriteLine("Write number of action that you want to do (write only number)");
                string choice = Console.ReadLine();
                if (MenuService.menu.Keys.Contains(choice))
                {
                    Console.WriteLine("Write id, that you want to get");
                    try
                    {
                        int id = Int32.Parse(Console.ReadLine());
                        Console.WriteLine(MenuService.menu[choice].Invoke(id));
                    }
                    catch
                    {
                        Console.WriteLine("Wrong input!");
                    }
                }
                else
                {
                    Console.WriteLine("Sorry, there is no options with such letter");
                }
            }
            
            Console.ReadLine();
        }
    }
}