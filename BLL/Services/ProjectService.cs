using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using ProjectStructure.BLL.Interfaces;
using ProjectStructure.DAL.Interfaces;
using ProjectStructure.DAL.Realization;
using ProjectStructure.Models.DTO.Project;
using ProjectStructure.Models.Entities;

namespace ProjectStructure.BLL.Services
{
    public class ProjectService : IProjectService
    {
        private IUnitOfWork _unitOfWork;
        private IMapper _mapper;
        private ITaskService _taskService;
        
        public ProjectService(IUnitOfWork unitOfWork, IMapper mapper, ITaskService taskService)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
            _taskService = taskService;
        }
        
        public void CreateProject(ProjectCreateDTO item)
        {
            Project project = _mapper.Map<Project>(item);
            project.CreatedAt = DateTime.Now;;
            project.Author = _unitOfWork.Users.Get(item.AuthorId);
            project.Team = _unitOfWork.Teams.Get(item.TeamId);
            if (project.Author == null || project.Team == null) throw new Exception();
            _unitOfWork.Projects.Create(project);
        }

        public void UpdateProject(ProjectUpdateDTO item)
        {
            _unitOfWork.Projects.Update(_mapper.Map<Project>(item));
        }

        public void DeleteProject(int id)
        {
            Project project = _unitOfWork.Projects.Get(id);
            if (project != null)
            {
                _unitOfWork.Tasks.GetAll().Where(t => t.Project == project).ToList().ForEach(t =>
                {
                    _taskService.DeleteTask(t.Id);
                });
                _unitOfWork.Projects.Delete(project);
            }
            else
            {
                throw new Exception();
            }
        }

        public ProjectReadDTO GetProject(int id)
        {
            return _mapper.Map<ProjectReadDTO>(_unitOfWork.Projects.Get(id));
        }

        public IEnumerable<ProjectReadDTO> GetAllProjects()
        {
            return _mapper.Map<IEnumerable<ProjectReadDTO>>(_unitOfWork.Projects.GetAll());
        }
    }
}