using ProjectStructure.Models.Entities;

namespace ProjectStructure.DAL.Interfaces
{
    public interface ITeamRepository : IRepository<Team>
    {
        
    }
}