using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using ProjectStructure.BLL.Interfaces;
using ProjectStructure.DAL.Interfaces;
using ProjectStructure.Models.DTO.Team;
using ProjectStructure.Models.Entities;

namespace ProjectStructure.BLL.Services
{
    public class TeamService : ITeamService
    {
        private IUnitOfWork _unitOfWork;
        private IMapper _mapper;
        private IProjectService _projectService;

        public TeamService(IUnitOfWork unitOfWork, IMapper mapper, IProjectService projectService)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
            _projectService = projectService;
        }
        
        public void CreateTeam(TeamCreateDTO team)
        {
            Team teamToCreate = _mapper.Map<Team>(team);
            teamToCreate.CreatedAt = DateTime.Now;
            _unitOfWork.Teams.Create(teamToCreate);
        }

        public void DeleteTeam(int Id)
        {
            Team team = _unitOfWork.Teams.Get(Id);
            if(team != null)
            {
                _unitOfWork.Users.GetAll().Where(u => u.Team == team).ToList().ForEach(u => u.Team = null);
                _unitOfWork.Projects.GetAll().Where(p => p.Team == team).ToList().ForEach(p =>
                {
                    _projectService.DeleteProject(p.Id);
                });
                _unitOfWork.Teams.Delete(team);
            }
            else
            {
                throw new Exception();
            }
            _unitOfWork.Save();
        }

        public void UpdateTeam(TeamUpdateDTO team)
        {
            Team teamToUpdate = _mapper.Map<Team>(team);
            _unitOfWork.Teams.Update(teamToUpdate);
        }

        public TeamReadDTO GetTeam(int Id)
        {
            return _mapper.Map<TeamReadDTO>(_unitOfWork.Teams.Get(Id));
        }

        public IEnumerable<TeamReadDTO> GetAllTeams()
        {
            return _mapper.Map<IEnumerable<TeamReadDTO>>(_unitOfWork.Teams.GetAll());
        }
    }
}