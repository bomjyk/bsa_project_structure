using System;
using System.Collections;
using System.Collections.Generic;
using AutoMapper;
using ProjectStructure.BLL.Interfaces;
using ProjectStructure.DAL.Interfaces;
using ProjectStructure.Models.DTO.User;
using ProjectStructure.Models.Entities;


namespace ProjectStructure.BLL.Services
{
    public class UserService : IUserService
    {
        private IMapper _mapper;
        private IUnitOfWork _unitOfWork;

        public UserService(IMapper mapper, IUnitOfWork unitOfWork)
        {
            _mapper = mapper;
            _unitOfWork = unitOfWork;
        }
        
        public void RegisterUser(UserCreateDTO user)
        {
            User registerUser = _mapper.Map<User>(user);
            registerUser.RegisteredAt = DateTime.Now;
            if(user.TeamId != null) {
                registerUser.Team = _unitOfWork.Teams.Get(user.TeamId.Value);
            }
            _unitOfWork.Users.Create(registerUser);
            _unitOfWork.Save();
        }

        public void UpdateUser(UserUpdateDTO user)
        {
            User updateUser = _mapper.Map<User>(user);
            _unitOfWork.Users.Update(updateUser);
            _unitOfWork.Save();
        }

        public UserReadDTO GetUser(int Id)
        {
            return _mapper.Map<User, UserReadDTO>(_unitOfWork.Users.Get(Id));
        }

        public IEnumerable<UserReadDTO> GetAllUsers()
        {
            return _mapper.Map<IEnumerable<User>, IEnumerable<UserReadDTO>>(_unitOfWork.Users.GetAll());
        }

        public void DeleteUser(int Id)
        {
            User user = _unitOfWork.Users.Get(Id);
            if(user != null)
            {
                _unitOfWork.Users.Delete(user);
            }
            else
            {
                throw new Exception();
            }
            _unitOfWork.Save();
        }
    }
}