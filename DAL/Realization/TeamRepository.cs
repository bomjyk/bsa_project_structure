using System.Collections.Generic;
using System.IO;
using System.Linq;
using ProjectStructure.DAL.Interfaces;
using ProjectStructure.Models.Entities;
using Newtonsoft.Json;

namespace ProjectStructure.DAL.Realization
{
    public class TeamRepository : ITeamRepository
    {
        private List<Team> _db;

        public TeamRepository()
        {
            _db = new List<Team>();
        }
        
        public IEnumerable<Team> GetAll()
        {
            return (IEnumerable<Team>) _db;
        }

        public Team Get(int Id)
        {
            return _db.Find(t => t.Id == Id);
        }

        public void Create(Team item)
        {
            item.Id = _db.Count;
            _db.Add(item);
        }

        public void Update(Team item)
        {
            var team = _db.FirstOrDefault(t => t.Id == item.Id);
            if (team != null) _db[_db.FindIndex(t => t.Id == item.Id)] = item;
        }

        public void Delete(Team item)
        {
            _db.Remove(item);
        }
    }
}