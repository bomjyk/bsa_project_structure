using System;
using System.Collections.Generic;

namespace LINQ.Models
{
    public class Project
    {
        public int Id { get; set; }
        public Team Team { get; set; }
        public User Author { get; set; }
        public List<Task> Tasks { get; set; } = new List<Task>();
        public string? Name { get; set; }
        public string? Description { get; set; }
        public DateTime Deadline { get; set; }
        public DateTime CreatedAt { get; set; }
        
        public Project() {}

        public Project(Project copy)
        {
            Id = copy.Id;
            Team = copy.Team;
            Author = copy.Author;
            Tasks = copy.Tasks;
            Name = copy.Name;
            Description = copy.Description;
            Deadline = copy.Deadline;
            CreatedAt = copy.CreatedAt;
        }
    }
}