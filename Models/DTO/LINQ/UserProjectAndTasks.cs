namespace ProjectStructure.Models.DTO.LINQ
{
    public class UserProjectAndTasks
    {
        public Entities.User User { get; set; }
        public Entities.Project LastProject { get; set; }
        public int TasksNumber { get; set; }
        public int NotFinishedTasks { get; set; }
        public Entities.Task LongestTask { get; set; }
    }
}