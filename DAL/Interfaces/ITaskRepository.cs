using ProjectStructure.Models.Entities;

namespace ProjectStructure.DAL.Interfaces
{
    public interface ITaskRepository : IRepository<Task>
    {
        
    }
}