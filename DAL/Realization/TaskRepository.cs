using System.Collections.Generic;
using System.IO;
using System.Linq;
using AutoMapper;
using Newtonsoft.Json;
using ProjectStructure.DAL.Interfaces;
using ProjectStructure.Models.DTO.Task;
using ProjectStructure.Models.Entities;

namespace ProjectStructure.DAL.Realization
{
    public class TaskRepository : ITaskRepository
    {
        private List<Task> _db;

        public TaskRepository()
        {
            _db = new List<Task>();
        }
        
        public IEnumerable<Task> GetAll()
        {
            return (IEnumerable<Task>) _db;
        }

        public Task Get(int Id)
        { 
            return _db.Find( t => t.Id == Id);
        }

        public void Create(Task item)
        {
            item.Id = _db.Count;
            _db.Add(item);
        }

        public void Update(Task item)
        {
            var task = _db.FirstOrDefault(t => t.Id == item.Id);
            if (task != null) _db[_db.FindIndex(t => t.Id == item.Id)] = item;
        }

        public void Delete(Task item)
        {
            _db.Remove(item);
        }
    }
}