namespace LINQ.Models
{
    public enum TaskState
    {
        Todo,
        InProgress,
        Done,
        Canceled
    }
}