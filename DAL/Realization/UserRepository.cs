using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Newtonsoft.Json;
using ProjectStructure.DAL.Interfaces;
using ProjectStructure.Models.Entities;

namespace ProjectStructure.DAL.Realization
{
    public class UserRepository : IUserRepository
    {
        private List<User> _db;
        public UserRepository()
        {
            _db = new List<User>();
        }
        public IEnumerable<User> GetAll()
        {
            return (IEnumerable<User>)_db.Select(u => u);
        }

        public User Get(int Id)
        {
            return _db.FirstOrDefault(u => u.Id == Id);
        }

        public void Create(User item)
        {
            item.Id = _db.Count;
            _db.Add(item);
        }

        public void Update(User item)
        {
            var user = _db.FirstOrDefault(u => u.Id == item.Id);
            if (user != null) _db[_db.FindIndex(t => t.Id == item.Id)] = item; // TODO: throws exception
        }

        public void Delete(User item)
        {
            _db.Remove(item);
        }
    }
}